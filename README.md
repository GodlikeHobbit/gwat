# GWAT

A trial project for Global Work & Travel Co

## Running the server

Run `npm start` it starts both the node server and the angular server

### Software:

Front end: AngularJS 2 (v4.4.5)

Back end: NodeJS

Database: MySQL

### Features:
- Retrieves all websites via GET request
- Post about new websites
- Submits via post request
- Vertical list of websites
- Newest posts first
- If a post has a url it is displayed in an iframe

Add Site button opens a modal which contains:
- Title
- Category
- Body
- Url

### Bonus:
☑ Display new website at top of page without reload once submitted

☑ Validation, Client-side

☑ Validation, Server-side

☑ SQL sanitation

☑ Pagination (3)

☑ Server does not crash


### Notes:

**Newest posts first:** In order to sort the websites by newest first, I simply reverse the array when retrieved. I chose to do this because it is slightly faster than:

```SQL
SELECT * FROM tablename
ORDER BY ID DESC;
```

**Client-side Validation:** The modal form has basic HTML5 validation, the url must be a url, everything except the url is required, the title must have a max length of 50 characters.

**Pagination:** I used a package to handle pagination as I couldn't really see a reason not to, if I had to implement it myself I would do something along the lines of:

```SQL
SELECT * FROM tablename
ORDER BY ID DESC 
LIMIT 'pageNo * 3', 3;
```

Clicking a set of dynamically generated buttons for each page (ceil(rowCount / 3)) would update the app-component's sites variable with the result of the sql query. Additionally, the implementation I chose had a poor template system so I chose not to style the pagination bar.

**Server does not crash:** I did not test this thoroughly, I don't think any input will break it though. 