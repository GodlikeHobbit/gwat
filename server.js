// Database constants
const dbHost = 'localhost';
const dbName = 'gwat_db';
const dbUser = 'root';
const dbPass = 'password1';

// Require ExpressJS
const express = require('express');
// Require MySQL driver
const mysql = require('mysql')
// Require body-parser
const bodyParser = require('body-parser')
// Require validator
const validator = require('validator');
// Express instance
const app = express();
// Database connection using database constants
const db = mysql.createConnection({
	host     : dbHost,
	user     : dbUser,
	password : dbPass,
	database : dbName
});
// Connect to the db
db.connect();

//Body Parser
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());

// Set headers to allow CORS requests
app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.header("Content-Security-Policy", "frame-ancestors 'none'");
	res.header("X-Frame-Options", "DENY");
	next();
});

/*
 * Get on root directory (/)
 * @returns {string} a temp html page
 */
app.get('/', function(req, res) {
	// Temp server result on get root directory
	res.send(`
		<p>Local Test Server</p>
		<hr>
		<p>Get all websites with 
			<br><span style="font-style: italic; font-weight: bold;">GET /websites</span>
		</p>
		<hr>
		<p>Upload a new website with 
			<br><span style="font-style: italic; font-weight: bold;">POST /websites/new</span>
		</p>
	`)
});

/*
 * Get on /websites
 * @returns {json} all websites to be displayed
 */
app.get('/websites', function(req, res) {
	// Get all sites
	// TODO: Pagination
	db.query('SELECT * FROM sites', function(err, rows, fields) {
		// Catch errors
		if (err) throw err;
		// Return result of query
		res.json(rows);
	});
});

/*
 * Get on root directory (/)
 * @param {Site} site the site to be added to the list
 * @returns {json} the site that was added
 */
app.post('/websites/new', function (req, res) {
	// Store data from request
	var data = req.body;
	var validCategories = ["1", "2", "3"];
	var validTitleLength = {
		min: 0,
		max: 50
	};
	/* Validation */
	// Title
	if (validator.isEmpty(data.title)) {res.status(400).send('Title cannot be empty');}
	if (!validator.isByteLength(data.title, validTitleLength)) {res.status(400).send('Title must be less than 50 bytes');}
	// Body
	if (validator.isEmpty(data.body)) {res.status(400).send('Body cannot be empty');}
	// Category
	if (!validator.isIn(data.category + '', validCategories)) {res.status(400).send('Category is invalid');}
	// URL
	if (!validator.isEmpty(data.url)) {
		// A URL was supplied
		if (!validator.isURL(data.url)) {res.status(400).send('URL is invalid');}
	}
	/* End validation */
	db.query('INSERT INTO sites SET ?', data, function(err, result) {
		if (err) throw err;
		res.send(req.body);
	});
});

app.listen(3000, function() {
	console.log('Test Server listening on port 3000');
});