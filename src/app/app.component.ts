import { Component, OnInit } 	from '@angular/core';
import { environment } 			from '../environments/environment';
import { SiteService } 			from './site/site.service';
import { Site } 				from './site/site';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
})


export class AppComponent implements OnInit {

	/**
	 * @type {string}
	 * The title of the page
	 */
	title = 'Global Work & Travel Co';
	/**
	 * @type {Site[]}
	 * The array of Site's to display
	 */
	sites: Site[];
	/**
	 * @type {boolean}
	 * Boolean to determine whether the modal is shown or not
	 */
	modal: boolean = false;
	/**
	 * @type {Site}
	 * The Site a user has added, to allow for displaying at the top without reloading
	 */
	addedSite: Site;
	/**
	 * @type {number}	
	 * The current page of sites the user is on
	 */
	pageNo: number = 1;

	/**
	 * @constructor
	 */
	constructor(private siteService: SiteService) { }
	
	/**
	 * Function that gets called on initialisation of AppComponent
	 * Retrives sites from the dbURL stored in the env
	 */
	ngOnInit(): void {
		this.getSites(environment.dbURL);
	};

	/**
	 * Function to access SiteService for accessing websites
	 * @param {string} dbURL the url to fetch the website data from
	 * @returns {void}
	 */
	getSites(dbURL: string): void {
		// Call SiteService getSites function then update sites
		this.siteService.getSites(dbURL).then(sites => {
			// Store sites
			var sites = sites;
			// Reverse sites to get newest first
			sites.reverse();
			// Update Apps sites
			this.sites = sites;
		});
	};

	/**
	 * Function to update whether the modal has been shown/hidden
	 * false == hidden, true == shown
	 * @param {boolean} modal whether the modal button has been clicked
	 * @returns {void}
	 */
	onClicked(modal: boolean): void {
		// Set the modal variable
		this.modal = modal;
	}

	/**
	 * Function to update the users added site
	 * @param {Site} site the site to be shown at the top of the list
	 * @returns {void}
	 */
	displaySite(site: Site): void {
		this.addedSite = site;
	}

}