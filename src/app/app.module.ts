import { BrowserModule } 			from '@angular/platform-browser';
import { NgModule } 				from '@angular/core';
import { FormsModule }	 			from '@angular/forms';
import { HttpModule } 					from '@angular/http';

import { AppComponent } 				from './app.component';
import { SiteComponent } 				from './site/site.component';
import { FormComponent } 				from './form/form.component';
import { ModalButtonComponent } 		from './modal-button/modal-button.component';
import { NgxPaginationModule } 			from 'ngx-pagination';
import { SiteService } 					from './site/site.service';

import { TrustPipe } 					from './trust.pipe';

@NgModule({
	declarations: [
		AppComponent,
		TrustPipe,
		SiteComponent,
		FormComponent,
		ModalButtonComponent
	],
	imports: [
		BrowserModule,
		FormsModule,
		HttpModule,
		NgxPaginationModule
	],
	providers: [SiteService],
	bootstrap: [AppComponent]
})

export class AppModule { }
