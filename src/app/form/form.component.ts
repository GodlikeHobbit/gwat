import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Site } from '../site/site';
import { SiteService } from '../site/site.service';
import { environment } from '../../environments/environment';

@Component({
	selector: 'app-form',
	templateUrl: './form.component.html',
})

export class FormComponent {
	
	/**
	 * @type {object}
	 * The object to hold data from the form
	 */
	newSite: object = {};

	/**
	 * @type {Site}
	 * The site to be displayed without needing to reload the page
	 */
	addedSite: Site;

	@Input('modal') modal: boolean = false;
	@Output() onClicked = new EventEmitter<boolean>();
	@Output() displaySite = new EventEmitter<Site>();

	constructor(private siteService: SiteService) { }

	/**
	 * Function to try to cast the data to a site 
	 * @param {any} data the site to be added to the database
	 * @returns {boolean} returns true if successful, false if not
	 */
	verifySite(value: any): boolean {
		// Error checking/validation
		try {
			var site: Site = value;
			this.addSite(site, environment.dbPostURL);
			return true;
		} catch(e) {
			console.log(e);
			return false;
		}
	}

	/**
	 * Function to asynchronously get sites via a Promise 
	 * @param {Site} data the site to be added to the database
	 * @param {string} dbURL the url to post the site data to
	 * @returns {void}
	 */
	addSite(data: Site, dbPostURL: string): void {
		// Call SiteService getSites function then update sites
		this.siteService.addSite(data, dbPostURL).then(returnedSite => {
			this.addedSite = returnedSite as Site;
	    	this.displaySite.emit(this.addedSite);
	    	this.onClicked.emit(!this.modal);
		});
	};
}
