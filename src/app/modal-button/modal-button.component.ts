import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
	selector: 'modal-button',
	templateUrl: './modal-button.component.html',
})

export class ModalButtonComponent {
	
	@Output() onClicked = new EventEmitter<boolean>();
	modal: boolean = false;

	constructor() { }
	
	addSite(): void {
	    this.modal = !this.modal;
	    this.onClicked.emit(this.modal);
	}
}