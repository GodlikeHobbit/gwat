import { Component, Input } from '@angular/core';
import { Site } from './site';

@Component({
	selector: 'app-site',
	templateUrl: './site.component.html',
})

export class SiteComponent {
	@Input() site: Site;
}