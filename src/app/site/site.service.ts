import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Site } from './site';

@Injectable()
export class SiteService {

	constructor(private http: Http) {}

	/**
	 * Function to asynchronously get sites via a Promise 
	 * @param {string} dbURL the url to fetch the website data from
	 * @returns {Promise<Site[]>} Promise for the sites to be displayed as a Site[]
	 */
	getSites(dbURL: string): Promise<Site[]> {
		// Return the result of a GET Request on dbURL
		return this.http.get(dbURL)
			.toPromise()
			.then(response => response.json() as Site[])
			.catch(this.handleError);
	}

	/**
	 * Function to asynchronously get sites via a Promise 
	 * @param {Site} data the site to be added to the database
	 * @param {string} dbURL the url to post the site data to
	 * @returns {Promise<Site>} Promise for the site to be added
	 */
	addSite(data: Site, dbPostURL: string): Promise<Site> {
		return this.http
			.post(dbPostURL, data)
			.toPromise()
			.then(res => res.json() as Site)
			.catch(this.handleError);
	}

	/**
	 * Function to handle errors via a Promise 
	 * @param {any} error The error to be handled
	 */
	private handleError(error: any): Promise<any> {
		return Promise.reject(error.message || error);
	}
}
