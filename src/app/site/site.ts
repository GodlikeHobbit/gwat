export class Site {
	id: number;
	title: string;
	body: string;
	category: number;
	url?: string;
}