import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser'

/**
 * Pipe for converting url to a SafeResourceUrl
 */
@Pipe({name: 'trust'})
export class TrustPipe implements PipeTransform {
	constructor(private sanitizer: DomSanitizer) {}

	/**
	 * Default pipe function that is called
	 *
	 * @param {string} url The url to be converted
	 * @returns {SafeResourceUrl} The safe version of url
	 */
	transform(url: string) {
		return this.sanitizer.bypassSecurityTrustResourceUrl(url);
	}
}